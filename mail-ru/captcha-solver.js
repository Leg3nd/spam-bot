'use strict';

const watson = require('watson-developer-cloud');
const fs = require('fs');

class CaptchaSolver {
  constructor() {
  }

  async run() {
    try {
      var visual_recognition = watson.visual_recognition({
        api_key: '772275d7f54cc091e9be7ef6698039e3c2d84ce8',
        version: 'v3',
        version_date: '2016-05-20'
      });

      var params = {
        images_file: fs.createReadStream('/Users/igoremykin/2.jpeg'),
        parameters: fs.createReadStream('../watson.json')
      };

      await new Promise((resolve, reject) => { visual_recognition.classify(params, function(err, res) {
        if (err) {
          console.log(err);
          reject(err);
        }
        else {
          console.log(JSON.stringify(res, null, 2));
          resolve(res);
        }
      })});

      // var params = {
      //   name: 'mail_ru_c2_captha',
      //   x_positive_examples: fs.createReadStream('/Users/igoremykin/x.zip'),
      //   c_positive_examples: fs.createReadStream('/Users/igoremykin/c.zip')
      // };
      //
      // visual_recognition.createClassifier(params,
      //   function(err, response) {
      //     if (err)
      //       console.log(err);
      //     else
      //       console.log(JSON.stringify(response, null, 2));
      //   });


      // visual_recognition.listClassifiers({},
      //   function(err, response) {
      //     if (err)
      //       console.log(err);
      //     else
      //       console.log(JSON.stringify(response, null, 2));
      //   }
      // );

      // visual_recognition.deleteClassifier({
      //     classifier_id: 'mail_ru_c2_captha_1033636211' },
      //   function(err, response) {
      //     if (err)
      //       console.log(err);
      //     else
      //       console.log(JSON.stringify(response, null, 2));
      //   }
      // );
    } catch(error) {
      // logger.error(error.message);
      console.log(error.message);
    }
  }
}

module.exports = CaptchaSolver;