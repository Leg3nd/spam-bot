import config from '../config';

class Api {
  _request(method, path, body) {
    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };

    const options = {method, headers};

    if (body) {
      options.body = JSON.stringify(body);
    }

    const url = `${config.endpoint}/${path}`;

    return fetch(url, options).then(this._json);
  }

  _json(res) {
    if (res.status === 200) {
      return res.json();
    } else {
      const error = new Error(res.statusText);
      error.response = res;
      throw error;
    }
  }

  _get(url) {
    return this._request('GET', url);
  }

  _post(url, body) {
    return this._request('POST', url, body);
  }

  _put(url, body) {
    return this._request('PUT', url, body);
  }

  async getFormats() {
    return await this._get('format');
  }

  async uploadAccounts(file, format) {
    let text = await this._getFileText(file);
    await this._post('account', {file: text, format});

    return {data: {file, text, format}, errors: []};
  }

  async getAccounts() {
    return await this._get('account');
  }

  async runRegistration() {
    return await this._post('account/run-registration');
  }

  async getSchedule() {
    return await this._get('schedule');
  }

  async saveSchedule(schedule) {
    return await this._put('schedule', {schedule});
  }

  async _getFileText(file) {
    var reader = new FileReader();
    reader.readAsText(file);

    return new Promise((resolve, reject) => {
      reader.onload = (event) => {
        return resolve(event.target.result);
      };

      reader.onerror = (event) => {
        return reject(event.target.error);
      };
    });
  }
}

export default new Api();