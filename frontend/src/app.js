import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route} from 'react-router';

import {store, history} from './redux/store';

// pages
import Home from './containers/home/Home';
import Registration from './containers/registration/Registration';
import Accounts from './containers/accounts/Accounts';

// styles
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vertical-tabs/bootstrap.vertical-tabs.min.css';

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={Home} />
      <Route path="/registration" component={Registration} />
      <Route path="/accounts" component={Accounts} />
    </Router>
  </Provider>,
  document.getElementById('root')
);