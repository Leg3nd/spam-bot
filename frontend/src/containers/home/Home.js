import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import UploadForm from '../../components/forms/UploadForm';
import UploadHistory from '../../components/controls/UploadHistory';
import Sidebar from '../../components/controls/Sidebar';
import * as accountActions from '../../redux/reducers/account';
import * as formatActions from '../../redux/reducers/format';

const uploadHistoryStyles = {position: 'absolute', right: '0px', top: '0px'};

const tabs = [
  {active: true, path: '/', text: 'Uploading'},
  {active: false, path: '/registration', text: 'Registration'},
  {active: false, path: '/accounts', text: 'Accounts'}
];

class Home extends Component {
  componentWillMount() {
    this.props.getFormats();
  }

  uploadHandler = ({file = [], format}) => {
    event.preventDefault();
    this.props.uploadAccounts(file[0], format);
  };

  render() {
    return (
      <div id="content">
        <Sidebar tabs={tabs} />
        <div className="col-xs-9">
          <div className="tab-content">
            <UploadForm
              onSubmit={this.uploadHandler}
              formats={this.props.formats}
              inProgress={this.props.inProgress}
            />
            <div style={uploadHistoryStyles}>
              <UploadHistory uploadResults={this.props.uploadResults}></UploadHistory>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    inProgress:    state.account.inProgress,
    uploadResults: state.account.uploadResults,
    formats:       state.format.data
  }),
  dispatch => ({
    ...bindActionCreators(accountActions, dispatch),
    ...bindActionCreators(formatActions, dispatch)
  })
)(Home);
