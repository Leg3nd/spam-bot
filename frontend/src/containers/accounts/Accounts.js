import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import config from '../../config';

import Sidebar from '../../components/controls/Sidebar';
import DownloadForm from '../../components/forms/DownloadForm';
import * as accountActions from '../../redux/reducers/account';
import * as formatActions from '../../redux/reducers/format';

const tabs = [
  {active: false, path: '/', text: 'Uploading'},
  {active: false, path: '/registration', text: 'Registration'},
  {active: true, path: '/accounts', text: 'Accounts'}
];

const statusIcon = {
  'NEW':         'glyphicon glyphicon-file',
  'IN_PROGRESS': 'glyphicon glyphicon-refresh',
  'SUCCESSFUL':  'glyphicon glyphicon-ok',
  'FAILED':      'glyphicon glyphicon-remove'
};

const statusStyle = {
  'NEW':         {backgroundColor: '#337ab7'},
  'IN_PROGRESS': {backgroundColor: '#f0ad4e'},
  'SUCCESSFUL':  {backgroundColor: '#5cb85c'},
  'FAILED':      {backgroundColor: '#d9534f'}
};

const statusText = {
  'NEW':         'New',
  'IN_PROGRESS': 'In progress',
  'SUCCESSFUL':  'Successful',
  'FAILED':      'Failed'
};

class Accounts extends Component {
  componentWillMount() {
    this.props.getAccounts();
    this.props.getFormats();
  }

  render() {
    return (
      <div id="content">
        <Sidebar tabs={tabs}/>
        <div className="col-xs-9">
          <div className="tab-content">
            <DownloadForm
              downloadUrl={`${config.endpoint}/account/download`}
              formats={this.props.formats}
              inProgress={this.props.inProgress}
            />
            <div className="table-responsive">
              <div className="tooltip top" role="tooltip">
                <div className="tooltip-arrow"></div>
                <div className="tooltip-inner">
                  Some tooltip text!
                </div>
              </div>
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Proxy</th>
                    <th className="text-center">Status</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.accounts.map((account, index) => {
                    return (
                      <tr key={index}>
                        <td>{account.email}</td>
                        <td>{account.username}</td>
                        <td>{account.pass}</td>
                        <td>{account.ip}:{account.ipPort}</td>
                        <td className="text-center">
                          <span className="badge" style={statusStyle[account.status]}>
                            {<span className={statusIcon[account.status]}></span>} {statusText[account.status]}
                          </span>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    inProgress: state.account.inProgress,
    accounts:   state.account.accounts,
    formats:    state.format.data
  }),
  dispatch => ({
    ...bindActionCreators(accountActions, dispatch),
    ...bindActionCreators(formatActions, dispatch)
  })
)(Accounts);
