import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Sidebar from '../../components/controls/Sidebar';
import ScheduleForm from '../../components/forms/ScheduleForm';
import * as scheduleActions from '../../redux/reducers/schedule';
import * as accountActions from '../../redux/reducers/account';

const wellStyles = {width: '600px', background: '#fff', paddingBottom: "4px"};

const tabs = [
  {active: false, path: '/', text: 'Uploading'},
  {active: true, path: '/registration', text: 'Registration'},
  {active: false, path: '/accounts', text: 'Accounts'}
];

const frequencyOptions = {
  'DAY':        'Day',
  'THREE_DAYS': 'Three days',
  'WEEK':       'Week',
  'MONTH':      'Month',
};

class Registration extends Component {
  componentWillMount() {
    this.props.getSchedule();
  }

  saveHandler = ({frequency, hours, minutes}) => {
    event.preventDefault();
    this.props.saveSchedule({frequency, hours, minutes});
  };

  render() {
    return (
      <div id="content">
        <Sidebar tabs={tabs}/>
        <div className="col-xs-9">
          <div className="tab-content">
            <ScheduleForm
              onSubmit={this.saveHandler}
              runRegistration={this.props.runRegistration}
              schedule={this.props.schedule}
              frequencyOptions={frequencyOptions}
              className="well upload-form"
              style={wellStyles}/>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    schedule: state.schedule
  }),
  dispatch => ({
    ...bindActionCreators(scheduleActions, dispatch),
    ...bindActionCreators(accountActions, dispatch)
  })
)(Registration);
