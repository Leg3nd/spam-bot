import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { reducer as form } from 'redux-form';

import account from './account';
import format from './format';
import schedule from './schedule';

export default combineReducers({
  routing,
  form,
  account,
  format,
  schedule
});