import api from '../../service/api';

const UPLOAD_ACCOUNTS_START       = 'UPLOAD_ACCOUNTS_START';
const UPLOAD_ACCOUNTS_DONE        = 'UPLOAD_ACCOUNTS_DONE';
const UPLOAD_ACCOUNTS_FAIL        = 'UPLOAD_ACCOUNTS_FAIL';
const DOWNLOAD_ACCOUNTS_START     = 'DOWNLOAD_ACCOUNTS_START';
const DOWNLOAD_ACCOUNTS_DONE      = 'DOWNLOAD_ACCOUNTS_DONE';
const DOWNLOAD_ACCOUNTS_FAIL      = 'DOWNLOAD_ACCOUNTS_FAIL';
const REGISTRATION_ACCOUNTS_START = 'REGISTRATION_ACCOUNTS_START';
const REGISTRATION_ACCOUNTS_DONE  = 'REGISTRATION_ACCOUNTS_DONE';
const REGISTRATION_ACCOUNTS_FAIL  = 'REGISTRATION_ACCOUNTS_FAIL';
const GET_ACCOUNTS_START          = 'GET_ACCOUNTS_START';
const GET_ACCOUNTS_DONE           = 'GET_ACCOUNTS_DONE';
const GET_ACCOUNTS_FAIL           = 'GET_ACCOUNTS_FAIL';

const initState = {uploadResults: [], accounts: []};

export default (state = initState, action) => {
  switch (action.type) {
    case UPLOAD_ACCOUNTS_START:
      return {
        ...state,
        inProgress: true
      };

    case UPLOAD_ACCOUNTS_DONE:
      return {
        ...state,
        inProgress: false,
        uploadResults: [
          action.payload.result.data,
          ...state.uploadResults
        ]
      };

    case UPLOAD_ACCOUNTS_FAIL:
      return {
        ...state,
        inProgress: false,
        errors: action.payload.result.errors
      };

    case DOWNLOAD_ACCOUNTS_START:
      return {
        ...state,
        inProgress: true
      };

    case DOWNLOAD_ACCOUNTS_DONE:
      return {
        ...state,
        inProgress: false
      };

    case DOWNLOAD_ACCOUNTS_FAIL:
      return {
        ...state,
        inProgress: false,
        errors: action.payload.result.errors
      };

    case REGISTRATION_ACCOUNTS_START:
      return {
        ...state,
        inProgress: true
      };

    case REGISTRATION_ACCOUNTS_DONE:
      return {
        ...state,
        inProgress: false,
        accounts: action.payload.result.data
      };

    case REGISTRATION_ACCOUNTS_FAIL:
      return {
        ...state,
        inProgress: false,
        errors: action.payload.result.errors
      };

    case GET_ACCOUNTS_START:
      return {
        ...state,
        inProgress: true
      };

    case GET_ACCOUNTS_DONE:
      console.log(action.payload.result.data);
      return {
        ...state,
        inProgress: false,
        accounts: action.payload.result.data
      };

    case GET_ACCOUNTS_FAIL:
      return {
        ...state,
        inProgress: false,
        errors: action.payload.result.errors
      };

    default:
      return state;
  }
}

export const uploadAccounts = (file, format) => ({
  types: [UPLOAD_ACCOUNTS_START, UPLOAD_ACCOUNTS_DONE, UPLOAD_ACCOUNTS_FAIL],
  payload: {
    result: api.uploadAccounts(file, format)
  }
});

export const downloadAccounts = (format) => ({
  types: [DOWNLOAD_ACCOUNTS_START, DOWNLOAD_ACCOUNTS_DONE, DOWNLOAD_ACCOUNTS_FAIL],
  payload: {
    result: api.downloadAccounts(format)
  }
});

export const runRegistration = () => ({
  types: [REGISTRATION_ACCOUNTS_START, REGISTRATION_ACCOUNTS_DONE, REGISTRATION_ACCOUNTS_FAIL],
  payload: {
    result: api.runRegistration()
  }
});

export const getAccounts = () => ({
  types: [GET_ACCOUNTS_START, GET_ACCOUNTS_DONE, GET_ACCOUNTS_FAIL],
  payload: {
    result: api.getAccounts()
  }
});