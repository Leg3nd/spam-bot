import api from '../../service/api';

const GET_SCHEDULE_START  = 'GET_SCHEDULE_START';
const GET_SCHEDULE_DONE   = 'GET_SCHEDULE_DONE';
const GET_SCHEDULE_FAIL   = 'GET_SCHEDULE_FAIL';
const SAVE_SCHEDULE_START = 'SAVE_SCHEDULE_START';
const SAVE_SCHEDULE_DONE  = 'SAVE_SCHEDULE_DONE';
const SAVE_SCHEDULE_FAIL  = 'SAVE_SCHEDULE_FAIL';

const initState = {hours: '00', minutes: '00', frequency: 'DAY'};

export default (state = initState, action) => {
  switch (action.type) {
    case GET_SCHEDULE_START:
      return {
        ...state,
        inProgress: true
      };

    case GET_SCHEDULE_DONE:
      return {
        ...state,
        inProgress: false,
        ...action.payload.result.data
      };

    case GET_SCHEDULE_FAIL:
      return {
        ...state,
        inProgress: false,
        errors: action.payload.result.errors
      };

    case SAVE_SCHEDULE_START:
      return {
        ...state,
        inProgress: true
      };

    case SAVE_SCHEDULE_DONE:
      return {
        ...state,
        inProgress: false,
        ...action.payload.result.data
      };

    case SAVE_SCHEDULE_FAIL:
      return {
        ...state,
        inProgress: false,
        errors: action.payload.result.errors
      };

    default:
      return state;
  }
}

export const getSchedule = () => ({
  types: [GET_SCHEDULE_START, GET_SCHEDULE_DONE, GET_SCHEDULE_FAIL],
  payload: {
    result: api.getSchedule()
  }
});

export const saveSchedule = (schedule) => ({
  types: [SAVE_SCHEDULE_START, SAVE_SCHEDULE_DONE, SAVE_SCHEDULE_FAIL],
  payload: {
    result: api.saveSchedule(schedule)
  }
});