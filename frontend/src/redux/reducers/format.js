import api from '../../service/api';

const GET_FORMATS_START = 'GET_FORMATS_START';
const GET_FORMATS_DONE  = 'GET_FORMATS_DONE';
const GET_FORMATS_FAIL  = 'GET_FORMATS_FAIL';

const initState = {data: []};

export default (state = initState, action) => {
  switch (action.type) {
    case GET_FORMATS_START:
      return {
        ...state,
        inProgress: true
      };

    case GET_FORMATS_DONE:
      return {
        ...state,
        inProgress: false,
        ...action.payload.result
      };

    case GET_FORMATS_FAIL:
      return {
        ...state,
        inProgress: false,
        errors: action.payload.result.errors
      };

    default:
      return state;
  }
}

export const getFormats = () => ({
  types: [GET_FORMATS_START, GET_FORMATS_DONE, GET_FORMATS_FAIL],
  payload: {
    result: api.getFormats()
  }
});