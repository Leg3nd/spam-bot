import {hashHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import asyncMiddleware from 'redux-async';

import reducer from './reducers';

const createStoreWithMiddleware = applyMiddleware(compose(asyncMiddleware, routerMiddleware))(
  window.devToolsExtension ? window.devToolsExtension()(createStore) : createStore
);

export const store = createStoreWithMiddleware(reducer);
export const history = syncHistoryWithStore(hashHistory, store);