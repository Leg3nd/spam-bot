import React, {PropTypes, Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';

import DropdownField from '../controls/DropdownField';

const wellStyles         = {width: '600px', background: '#fff', paddingBottom: "4px"};
const sectionTitleStyles = {marginTop: '0px', marginBottom: '5px', color: '#aaa'};
const timeStyles         = {width: '60px', display: 'inline-block'};

const range = (min, max) => {
  const twoDigit = (value) => {
    return value.length < 2 ? '0' + value : value;
  };

  return (value, previousValue) => {
    return value <= max && value >= min ? twoDigit(value) : previousValue;
  }
};

class ScheduleForm extends Component {
  render() {
    return (
      <div className="well upload-form" style={wellStyles}>
        <form onSubmit={this.props.handleSubmit} className="form-horizontal">
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="button" className="btn btn-success" onClick={this.props.runRegistration}>
                Run immediately
              </button>
            </div>
          </div>
          <hr/>
          <div className="form-group">
            <label className="col-sm-2 control-label"></label>
            <div className="col-sm-10">
              <h4 style={sectionTitleStyles}>Schedule</h4>
            </div>
          </div>
          <div className="form-group">
            <label className="col-sm-2 control-label">Every</label>
            <div className="col-sm-10">
              <Field
                component={DropdownField}
                name="frequency"
                frequency={this.props.schedule.frequency}
                frequencyOptions={this.props.frequencyOptions}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="col-sm-2 control-label">At</label>
            <div className="col-sm-10">
              <Field
                component="input"
                name="hours"
                type="number"
                className="form-control"
                style={timeStyles}
                normalize={range(0, 23)}
              />
              <span> : </span>
              <Field
                component="input"
                name="minutes"
                type="number"
                className="form-control"
                style={timeStyles}
                normalize={range(0, 59)}
              />
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="submit" className="btn btn-primary">
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

var form = reduxForm({
  form:               'scheduleForm',
  enableReinitialize: true
})(ScheduleForm);

form = connect(
  (state, props) => ({
    initialValues: {
      hours:     props.schedule.hours,
      minutes:   props.schedule.minutes,
      frequency: props.schedule.frequency
    }
  }), {}
)(form);

export default form