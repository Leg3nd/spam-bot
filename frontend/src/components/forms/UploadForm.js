import React, {PropTypes, Component} from 'react';
import {Field, reduxForm} from 'redux-form';

import FileInput from '../controls/FileInput';

const wellStyles    = {width: '600px', background: '#fff', paddingBottom: "4px"};
const spinnerStyles = {marginLeft: '15px', paddingLeft: '37px'};
const dlStyles      = {marginBottom: "0px"};
const dtStyles      = {width: "60px"};
const ddStyles      = {marginLeft: "80px"};

class UploadForm extends Component {
  showRecent = (event) => {
    this.recentFormats.style.display = this.recentFormats.style.display === "inherit" ? "none" : "inherit";
  };

  recentClick = (event) => {
    this.props.change('format', event.target.innerText);
    this.recentFormats.style.display = "none";
  };

  render() {
    return (
      <div className="well upload-form" style={wellStyles}>
        <form onSubmit={this.props.handleSubmit} className="form-horizontal">
          <div className="form-group">
            <label className="col-sm-2 control-label">File</label>
            <div className="col-sm-10">
              <Field
                component={FileInput}
                btnText="Browse..."
                name="file"
                required="true"
                inProgress={this.props.inProgress}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="col-sm-2 control-label">Format</label>
            <div className="col-sm-10">
              <div className="input-group">
                <Field
                  component="input"
                  type="string"
                  name="format"
                  placeholder="For example: email;e_pass;ip_addr;"
                  required="true"
                  className="form-control"
                  disabled={this.props.inProgress}
                />
                <div className="input-group-btn">
                  <button
                    type="button"
                    disabled={!this.props.formats.length}
                    className="btn btn-default dropdown-toggle"
                    onClick={this.showRecent}
                  >
                    Recent <span className="caret"></span>
                  </button>
                  <ul className="dropdown-menu dropdown-menu-right" ref={(ul) => {this.recentFormats = ul;}}>
                    {this.props.formats.slice(0, 10).map((entry, index) => {
                      return (
                        <li key={index}>
                          <a href="#" onClick={this.recentClick}>
                            {entry.value}
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <div className="alert alert-warning" role="alert">
                <strong>Please, specify file format with next types:</strong>
                <br/>
                <br/>
                <dl className="dl-horizontal" style={dlStyles}>
                  <dt style={dtStyles}>email</dt>
                  <dd style={ddStyles}>Email address <strong>*</strong></dd>

                  <dt style={dtStyles}>e_pass</dt>
                  <dd style={ddStyles}>Email password <strong>*</strong></dd>

                  <dt style={dtStyles}>ip_addr</dt>
                  <dd style={ddStyles}>IP address <strong>*</strong></dd>

                  <dt style={dtStyles}>ip_pass</dt>
                  <dd style={ddStyles}>IP password</dd>

                  <dt style={dtStyles}>ip_login</dt>
                  <dd style={ddStyles}>IP login</dd>
                </dl>
                <br/>
                <strong>*</strong> mandatory field
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="submit" className="btn btn-success" disabled={this.props.inProgress}>
                Upload
              </button>
              <label className={this.props.inProgress ? "" : "hidden"} style={spinnerStyles}>
                <div className="spinner"></div>
                Uploading...
              </label>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'uploadForm'
})(UploadForm);