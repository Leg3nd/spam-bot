import React, {PropTypes, Component} from 'react';
import {Field, reduxForm} from 'redux-form';

const wellStyles    = {width: '705px', background: '#fff', paddingBottom: "4px"};
const submitStyles  = {float: 'right'};
const formatStyles  = {width: 'calc(100% - 105px)', display: 'inline-table'};

class DownloadForm extends Component {
  showRecent = (event) => {
    this.recentFormats.style.display = this.recentFormats.style.display === "inherit" ? "none" : "inherit";
  };

  recentClick = (event) => {
    this.props.change('format', event.target.innerText);
    this.recentFormats.style.display = "none";
  };

  render() {
    return (
      <div className="well download-form" style={wellStyles}>
        <form action={this.props.downloadUrl} method="GET" className="form-horizontal">
          <div className="form-group">
            <label className="col-sm-2 control-label">Format</label>
            <div className="col-sm-10">
              <div className="input-group" style={formatStyles}>
                <Field
                  component="input"
                  type="string"
                  name="format"
                  placeholder="For example: username;u_pass;ip_addr;"
                  required="true"
                  className="form-control"
                  disabled={this.props.inProgress}
                />
                <div className="input-group-btn">
                  <button
                    type="button"
                    disabled={!this.props.formats.length}
                    className="btn btn-default dropdown-toggle"
                    onClick={this.showRecent}
                  >
                    Recent <span className="caret"></span>
                  </button>
                  <ul className="dropdown-menu dropdown-menu-right" ref={(ul) => {this.recentFormats = ul;}}>
                    {this.props.formats.slice(0, 10).map((entry, index) => {
                      return (
                        <li key={index}>
                          <a onClick={this.recentClick}>
                            {entry.value}
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
              <button type="submit" className="btn btn-success" disabled={this.props.inProgress} style={submitStyles}>
                Download
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'downloadForm'
})(DownloadForm);