import React, {PropTypes, Component} from 'react';

export default class DropdownField extends Component {
  select = (event) => {
    event.preventDefault();
    this.props.input.onChange(event.target.getAttribute('value'));
    this.selector.style.display = "none";
  };

  showSelector = (event) => {
    this.selector.style.display = this.selector.style.display === "inherit" ? "none" : "inherit";
  };

  render() {
    return (
      <div className="dropdown">
        <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" onClick={this.showSelector}>
          {this.props.frequencyOptions[this.props.input.value]} <span className="caret"></span>
        </button>
        <ul className="dropdown-menu" aria-labelledby="dropdownMenu1" ref={(ul) => {this.selector = ul;}}>
          {Object.keys(this.props.frequencyOptions).map((entry, index) => {
            return (
              <li key={index}>
                <a href="#" value={entry} onClick={this.select}>
                  {this.props.frequencyOptions[entry]}
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}