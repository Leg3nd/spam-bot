import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router';

const panelStyles = {width: "200px"};

export default class UploadHistory extends Component {
  render() {
    return (
      <div className="col-xs-3" style={panelStyles}>
        <ul className="nav nav-tabs tabs-left">
          {this.props.tabs.map((entry, index) => {
            return (
              <li className={entry.active ? 'active' : ''} key={index}>
                <Link to={entry.path} data-toggle="tab">
                  {entry.text}
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

UploadHistory.PropTypes = {
  tabs: PropTypes.array
};