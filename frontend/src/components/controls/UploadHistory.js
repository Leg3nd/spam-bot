import React, {PropTypes, Component} from 'react';

const panelStyles        = {width: "350px"};
const panelHeadingStyles = {paddingTop: "5px", paddingBottom: "5px"};
const panelTitleStyles   = {fontSize: "14px"};
const dlStyles           = {marginBottom: "0px"};
const dtStyles           = {width: "60px"};
const ddStyles           = {marginLeft: "80px"};

export default class UploadHistory extends Component {
  render() {
    return (
      <div>
        {this.props.uploadResults.slice(0, 10).map((entry, index) => {
          return (
            <div className="panel panel-success" key={index} role="alert" style={panelStyles}>
              <div className="panel-heading" style={panelHeadingStyles}>
                <h3 className="panel-title" style={panelTitleStyles}>The file was uploaded successfully:</h3>
              </div>
              <div className="panel-body">
                <dl className="dl-horizontal" style={dlStyles}>
                  <dt style={dtStyles}>Name</dt>
                  <dd style={ddStyles}>{entry.file.name}</dd>

                  <dt style={dtStyles}>Size</dt>
                  <dd style={ddStyles}>{entry.file.size} bytes</dd>

                  <dt style={dtStyles}>Format</dt>
                  <dd style={ddStyles}>{entry.format}</dd>
                </dl>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

UploadHistory.PropTypes = {
  uploadedFiles: PropTypes.array
};