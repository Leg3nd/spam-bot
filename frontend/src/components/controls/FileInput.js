import React, {PropTypes, Component} from 'react';

const fileNameStyles = {marginLeft: '15px', fontWeight: 'normal'};
const inputStyles = {opacity: "0", width: "85px", height: "1px", bottom: "0px", left: "12px", position: "absolute"};

export default class FileInput extends Component {
  constructor(props) {
    super(props);
    this.state = {fileName: ''};
  }

  fileChanged(event) {
    event.preventDefault();
    this.setState({fileName: event.target.files.length ? event.target.files[0].name : ''});
    this.props.input.onChange(event);
  }

  render() {
    this.props.input.value = undefined;

    return (
      <div>
        <label className="btn btn-default btn-file" disabled={this.props.inProgress}>
          {this.props.btnText}
          <input
            type='file'
            {...this.props.input}
            onChange={this.fileChanged.bind(this)}
            required={this.props.required}
            disabled={this.props.inProgress}
            style={inputStyles}
          />
        </label>
        <label style={fileNameStyles} ref='fileLabel'>{this.state.fileName}</label>
      </div>
    );
  }
}

FileInput.PropTypes = {
  btnText: PropTypes.string
};