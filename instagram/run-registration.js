"use strict";

const Registrar = require('./registrar');

var options = process.argv.slice(2).reduce((result, item) => {
  let elements = item.split('=');
  result[elements[0]] = elements[1];
  return result;
}, {});

let registrar = new Registrar(options);

registrar.run();