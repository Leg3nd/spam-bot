'use strict';

const webdriver = require('selenium-webdriver');
const chromedriver = require('chromedriver');

const By = webdriver.By;
const until = webdriver.until;

const PHONE_OR_EMAIL_TEXTBOX_XPATH = '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[2]/input';
const NAME_SURNAME_TEXTBOX_XPATH = '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[3]/input';
const USERNAME_TEXTBOX_XPATH = '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[4]/input';
const PASSWORD_TEXTBOX_XPATH = '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[5]/input';
const REGISTRATION_BTN_XPATH = '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[6]/span/button';
const WAIT_TIME = 120 * 1000;

class Registrar {
  constructor(options = {}) {
    options.proxyPort = options.proxyPort || '80';

    this.email    = options.email;
    this.password = options.password;
    this.name     = options.name;
    this.surname  = options.surname;
    this.username = options.username;

    this.driver = new webdriver.Builder();
    this.driver = options.proxyHost ?
      this.driver.setProxy({proxyType: "manual", sslProxy: `${options.proxyHost}:${options.proxyPort}`}) :
      this.driver;
    this.driver = this.driver.forBrowser('chrome');
    this.driver = this.driver.build();
  }

  async run() {
    try {
      await this.driver.get('https://www.instagram.com/');
      await this._register();
      await this.driver.sleep(5000);
      await this.driver.close();
    } catch(error) {
      // logger.error(error.message);
      console.log(error.message);
    } finally {
      await this.driver.quit();
    }
  }

  async _register() {
    // logger.info('Login');
    console.log('register');
    await this.driver.wait(until.elementLocated(By.xpath(PHONE_OR_EMAIL_TEXTBOX_XPATH)), WAIT_TIME);
    await this._sendKeys(PHONE_OR_EMAIL_TEXTBOX_XPATH, this.email);
    await this._sendKeys(NAME_SURNAME_TEXTBOX_XPATH, `${this.name} ${this.surname}`);
    await this._sendKeys(USERNAME_TEXTBOX_XPATH, this.username);
    await this._sendKeys(PASSWORD_TEXTBOX_XPATH, this.password);
    await this.driver.sleep(1500);
    await this.driver.findElement(By.xpath(REGISTRATION_BTN_XPATH)).click();
    await this.driver.sleep(15000);
  }

  async _sendKeys(xpath, keys) {
    keys = keys.split('');

    await this.driver.findElement(By.xpath(xpath)).click();
    await this.driver.sleep(this._getRandomNumber(150, 350));

    for (let key of keys) {
      await this.driver.findElement(By.xpath(xpath)).sendKeys(key);
      await this.driver.sleep(this._getRandomNumber(150, 350));
    }
  }

  _getRandomNumber(min, max) {
    return Math.random() * (max - min) + min;
  }
}

module.exports = Registrar;