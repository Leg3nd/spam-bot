'use strict';

const {Router} = require('express');
const controller = require('./account.controller');

const router = Router();

router.post('/', controller.call('upload'));
router.get('/download', controller.call('download'));
router.get('/', controller.call('getAccounts'));
router.post('/run-registration/', controller.call('runRegistration'));

module.exports = router;
