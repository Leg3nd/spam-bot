'use strict';

const typeScheme = require('../type-scheme');
const childProcess = require('child_process');
const rp = require('request-promise');
const Imap = require('imap');
const generator = require('generate-password');
const faker = require('faker');

faker.locale = 'en_US';

const CONFIRM_URL = 'https://instagram.com/accounts/confirm_email/';

module.exports = (sequelize, DataTypes) => {
  let model = sequelize.define('account', {
    id: {
      type:          DataTypes.INTEGER,
      primaryKey:    true,
      autoIncrement: true
    },
    email: {
      type:      DataTypes.STRING(50),
      allowNull: false,
      validate: {
        isEmail: true
      },
      set: function(value) {
        this.setDataValue('email', value.toLowerCase());
      }
    },
    emailPassword: {
      type:      DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    name: {
      type: DataTypes.STRING(50),
      validate: {
        notEmpty: true
      }
    },
    surname: {
      type: DataTypes.STRING(50),
      validate: {
        notEmpty: true
      }
    },
    username: {
      type: DataTypes.STRING(50),
      validate: {
        notEmpty: true
      }
    },
    pass: {
      type: DataTypes.STRING(50)
    },
    ip: {
      type:      DataTypes.STRING(50),
      allowNull: false,
      validate: {
        isIP: true
      }
    },
    ipPort: {
      type:      DataTypes.INTEGER,
      validate: {
        isNumeric: true
      }
    },
    ipLogin: {
      type: DataTypes.STRING(50),
      validate: {
        notEmpty: true
      }
    },
    ipPassword: {
      type: DataTypes.STRING(50),
      validate: {
        notEmpty: true
      }
    },
    status: {
      type:         DataTypes.ENUM('NEW', 'IN_PROGRESS', 'SUCCESSFUL', 'FAILED'),
      defaultValue: 'NEW',
      allowNull: false
    }
  }, {
    freezeTableName: true,
    timestamps:      true,
    underscored:     false,
    classMethods: {
      createFromText: async function(text, format) {
        return Promise.all(text.split('\n').map(async (str) => {
          return await this.createFromString(str, format);
        }));
      },
      createFromString: async function(str, format) {
        let tokenPositions = this.getTokenPositions(format);
        let values         = this.getValues(str, format, tokenPositions);
        let account        = this.castToModel(values);
        account.pass       = generator.generate({length: 10, numbers: true});
        account.name       = faker.name.firstName();
        account.surname    = faker.name.lastName();
        account.username   = `${account.name}${account.surname}${account.pass.slice(account.pass.length / 2)}`;

        return this.create(account);
      },
      getTokenPositions: function(format) {
        let res = Object.keys(typeScheme).reduce((result, type) => {
          let start = format.indexOf(type);

          if (~start) {
            result.push({type, start, end: start + type.length})
          }

          return result;
        }, []);

        res.sort((a, b) => {
          return a.start - b.start;
        });

        return res;
      },
      getValues: function(str, format, tokenPositions) {
        let res = {};
        let opt = {str};

        for (let i = 0, current = 0; i < tokenPositions.length; ++i) {
          let currentToken = tokenPositions[i];
          let nextToken    = tokenPositions[i + 1];

          opt.startSeparator = format.slice(current, currentToken.start);
          opt.endSeparator   = format.slice(currentToken.end, nextToken ? nextToken.start : format.length);

          res[currentToken.type] = this.extractValue(opt);

          current = currentToken.end;
        }

        return res;
      },
      extractValue: function(opt) {
        let start = opt.str.indexOf(opt.startSeparator);
        let end   = opt.endSeparator != '' ?
          opt.str.indexOf(opt.endSeparator, start + opt.startSeparator.length) :
          opt.str.length;
        let res   = opt.str.slice(start + opt.startSeparator.length, end);
        opt.str   = opt.str.slice(end, opt.str.length);

        return res;
      },
      castToModel: function(values) {
        let res = {};

        Object.keys(values).map((key) => {
          res[typeScheme[key]] = values[key];
        });

        res = this.handleIP(res);

        return res;
      },
      handleIP: function(model) {
        if (~Object.keys(model).indexOf('ip')) {
          let ipArray = model.ip.split(':');

          model.ip = ipArray[0];

          let ipPort = Number(ipArray[1]);
          ipPort && (model.ipPort = ipPort);
        }

        return model;
      },
      runRegistration: async function(account) {
        return new Promise((resolve, reject) => {
          let invoked = false;
          let process = childProcess.fork('./instagram/run-registration.js',
            [
              'email=' + account.email,
              'password=' + account.pass,
              'proxyHost=' + account.ip,
              'proxyPort=' + account.ipPort,
              'name=' + account.name,
              'surname=' + account.surname,
              'username=' + account.username
            ]);

          process.on('error', (error) => {
            if (invoked) {
              return;
            }

            invoked = true;
            reject(error);
          });

          process.on('exit', (code) => {
            if (invoked) {
              return;
            }

            invoked = true;
            code === 0 ? resolve() : reject(new Error('exit code ' + code));
          });
        });
      },
      getString: async function(format, status) {
        let accounts = await this.findAll({where: {'status': status}, raw: true});

        return accounts.reduce((result, account) => {
          result += result === '' ? '' : '\n';

          let str = format;
          str = str.replace('email', account.email);
          str = str.replace('e_pass', account.emailPassword);
          str = str.replace('ip_addr', `${account.ip}:${account.ipPort}`);
          str = str.replace('ip_pass', account.ipPassword);
          str = str.replace('ip_login', account.ipLogin);
          str = str.replace('username', account.username);
          str = str.replace('u_pass', account.pass);

          result += str;

          return result;
        }, '');
      },
      confirmRegistration: async function(account) {
        let letter = await new Promise((resolve, reject) => {
          let imap = new Imap({
            user:     account.email,
            password: account.emailPassword,
            host:     'imap.yandex.ru',
            port:     993,
            tls:      true
          });

          imap.once('ready', () => {
            imap.openBox('INBOX', true, (err, box) => {
              imap.search([['SUBJECT', 'Instagram']], (err, results) => {
                if (err) {
                  return reject(err);
                }

                let f = imap.fetch(results, {bodies: ''});

                f.on('message', (msg, seqno) => {
                  msg.on('body', (stream, info) => {
                    let buffer = '';

                    stream.on('data', (chunk) => {
                      buffer += chunk.toString('utf8');
                    });

                    stream.once('end', () => {
                      resolve(buffer);
                    });
                  });
                });

                f.once('error', (err) => {
                  reject(err);
                });
              });
            });
          });

          imap.once('error', (err) => {
            reject(err);
          });

          imap.connect();
        });

        let beginSlice = letter.indexOf(CONFIRM_URL);
        let endSlice = letter.indexOf('"', beginSlice);
        let confirmUrl = letter.slice(beginSlice, endSlice);
        confirmUrl = confirmUrl.replace(/\r|\n/g, '');

        await rp(confirmUrl);
      }
    }
  });

  return model;
};
