'use strict';

const bluebird = require('bluebird');
const sleep = require('thread-sleep');
const Response = require('../response');
let models = require('../sequelize');

const NUMBER_OF_PARALLEL_REGISTRATIONS = 3;

class AccountController extends Response {
  async upload(req, res) {
    try {
      await models.account.createFromText(req.body.file, req.body.format);
      await models.format.findOrCreate({
        where:    {value: req.body.format},
        defaults: {value: req.body.format}
      });

      return {data: null};
    } catch (error) {
      error.errors && res.status(400).json({errors: error.errors.map((item) => {
        return {field: item.path, message: item.message};
      })});
    }
  }

  async download(req, res) {
    try {
      await models.format.findOrCreate({
        where:    {value: req.query.format},
        defaults: {value: req.query.format}
      });

      let accounts = await models.account.getString(req.query.format, 'SUCCESSFUL');

      res.set({
        "Content-Disposition":"attachment; filename=\"accounts.txt\"",
        'Content-Type': 'application/force-download'
      });

      res.send(accounts);
    } catch (error) {
      error.errors && res.status(400).json({errors: error.errors.map((item) => {
        return {field: item.path, message: item.message};
      })});
    }
  }

  async getAccounts(req, res) {
    try {
      return {data: await models.account.findAll()};
    } catch (error) {
      error.errors && res.status(400).json({errors: error.errors.map((item) => {
        return {field: item.path, message: item.message};
      })});
    }
  }

  async runRegistration(req, res) {
    try {
      let accounts = await models.account.findAll({where: {'status': 'NEW'}, raw: true});
      let accountsIds = accounts.map((account) => {return account.id;});
      await models.account.update({'status': 'IN_PROGRESS'}, {where: {'id': accountsIds}});

      for (let i = 0; i < accounts.length; i += NUMBER_OF_PARALLEL_REGISTRATIONS) {
        await Promise.all(accounts.slice(i, i + NUMBER_OF_PARALLEL_REGISTRATIONS).map(async (account) => {
          try {
            await models.account.runRegistration(account);
            sleep(10000);
            await models.account.confirmRegistration(account);
            await models.account.update({'status': 'SUCCESSFUL'}, {where: {'id': account.id}});
          } catch (error) {
            await models.account.update({'status': 'FAILED'}, {where: {'id': account.id}});
          }
        }));
      }

      return {data: await models.account.findAll()};
    } catch (error) {
      error.errors && res.status(400).json({errors: error.errors.map((item) => {
        return {field: item.path, message: item.message};
      })});
    }
  }
}

module.exports = new AccountController();
