'use strict';

const { Router } = require('express');
const accountRouter = require('./account/account.router');
const formatRouter = require('./format/format.router');
const scheduleRouter = require('./schedule/schedule.router');

const router = Router();

router.use('/account', accountRouter);
router.use('/format', formatRouter);
router.use('/schedule', scheduleRouter);

module.exports = router;
