'use strict';

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const compression = require('compression');
const path = require('path');
const fs = require('fs');
const consolidate = require('consolidate');
const config = require('./config');
const api = require('./api');

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: '100mb'}));
app.use(compression({threshold: 0}));
app.use(express.static(path.resolve('./frontend/public/')));
app.engine('html', consolidate['swig']);
app.set('view engine', 'html');
app.set('views', path.resolve('./frontend/src/'));
app.use('/api', api);
app.get('/', (req, res) => {
  let assets = fs.readFileSync(path.resolve('./frontend/public/build/assets.json'), 'utf-8');
  assets = JSON.parse(assets);
  res.render('index', {assets})
});
app.use((error, req, res, next) => {
  let err = {
    message: error.toString()
  };

  if (error.name === 'ValidationError') {
    err = {
      message: error.name,
      errors:  error.errors
    }
  } else {
    console.error('Error: ', error);
  }

  res.status(500).json(err);
});

app.listen(config.port, () => console.log('Server started on port: ', config.port));
