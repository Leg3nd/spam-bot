'use strict';

/**
      {
        "serverBuildScheme": {
          "scheme": {
            "type": "Server",
            "injections": ["CallboardRouter", "UserRouter"],
            "metatypes": {
              "CallboardRouter": {
                "type": "CommonRouter",
                "injections": ["CallboardFacade"]
              },
              "UserRouter": {
                "type": "CommonRouter",
                "injections": ["UserFacade"]
              },
              "CommonRouter": {
                "type": "Router",
                "injections": [
                  {
                    "type": "Validator",
                    "injections": [
                      {
                        "type": "AuthChecker"
                      }
                    ]
                  }
                ]
              }
            }
          },
          "types": {
            "Server": "./ramote-access-layer/server.js",
            "Router": "./remote-access-layer/backend/router.js",
            "Validator": "./validation-layer/validator.js",
            "AuthChecker": "./protection-layer/auth-checker.js",
            "CallboardFacade": "./business-logic-layer/callboard-facade.js",
            "UserFacade": "./business-logic-layer/user-facade.js"
          }
        }
      }
 */

class CallInterceptor {
  constructor(source) {
    const self = this;

    return new Proxy(source, {
      get: function(target, property, receiver) {
        if (typeof target[property] === 'function') {
          return function(...args) {
            self.before.apply(self, [property].concat(args));

            typeof self[property] === 'function' && self[property].apply(self, args);
            let result = target[property].apply(target, args);

            self.after.apply(self, [property].concat(args));

            return result;
          }
        }

        return target[property];
      }
    });
  }

  before(methodName) {

  }

  after(methodName) {

  }
}

module.exports = CallInterceptor;