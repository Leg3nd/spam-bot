'use strict';

const typeScheme = require('../type-scheme');

module.exports = (sequelize, DataTypes) => {
  let model = sequelize.define('format', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    value: {
      type: DataTypes.STRING(200),
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: true
      },
      set: function(value) {
        this.setDataValue('value', value.toLowerCase());
      }
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    underscored: false,
    validate: {
      stringContainsTypes: function() {
        let validString = Object.keys(typeScheme).reduce((result, item) => {
          if (~this.value.indexOf(item)) {
            result = true;
          }

          return result;
        }, false);

        if (!validString) {
          throw new Error(`Format string should contains at least one of the next types: ${Object.keys(typeScheme).join(', ')}`);
        }
      }
    }
  });

  return model;
};
