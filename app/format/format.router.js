'use strict';

const {Router} = require('express');
const controller = require('./format.controller');

const router = Router();

router.get('/', controller.call('getFormats'));

module.exports = router;
