'use strict';

const Response = require('../response');
let models = require('../sequelize');

class FormatController extends Response {
  async getFormats(req, res) {
    try {
      return {data: await models.format.findAll()};
    } catch (error) {
      res.status(400).json({errors: error.errors.map((item) => {
        return {field: item.path, message: item.message};
      })});
    }
  }
}

module.exports = new FormatController();
