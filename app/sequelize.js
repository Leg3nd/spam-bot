'use strict';

const config = require('./config');
const path = require('path');
const _ = require('lodash');
const glob = require('glob');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(config.db.uri, config.db.options);

function setModels(db) {
  var models = {};

  // Globbing model files
  getGlobbedFiles(path.resolve('./app/**/*.model.js')).forEach((modelPath) => {
    let model = db.import(path.resolve(modelPath));
    models[model.name] = model;
  });

  models.sequelize = db;

  // Associate in models
  Object.keys(models).forEach((name) => {
    if ('associate' in models[name]) {
      models[name].associate(models);
    }
  });

  db.sync();

  return models;
}

function getGlobbedFiles(globPatterns, removeRoot) {
  // URL paths regex
  var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

  // The output array
  var output = [];

  // If glob pattern is array so we use each pattern in a recursive way, otherwise we use glob
  if (_.isArray(globPatterns)) {
    globPatterns.forEach(function (globPattern) {
      output = _.union(output, getGlobbedFiles(globPattern, removeRoot));
    });
  } else if (_.isString(globPatterns)) {
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns);
    } else {
      let files = glob(globPatterns, {sync: true});

      if (removeRoot) {
        files = files.map((file) => {
          return file.replace(removeRoot, '');
        });
      }

      output = _.union(output, files);
    }
  }

  return output;
}

module.exports = setModels(sequelize);

