'use strict';

const typeScheme = {
  'email':    'email',
  'e_pass':   'emailPassword',
  'ip_addr':  'ip',
  'ip_pass':  'ipPassword',
  'ip_login': 'ipLogin',
  'username': 'username'
};

module.exports = typeScheme;