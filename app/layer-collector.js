'use strict';

const path = require('path');

/**
  "serverBuildScheme": {
      "type":         "Server",
      "injections":   ["CallboardRouter", "UserRouter"],
      "compositions": {
        "CallboardRouter": {
          "type":       "CommonRouter",
          "injections": ["CallboardFacade"]
        },
        "UserRouter":      {
          "type":       "CommonRouter",
          "injections": ["UserFacade"]
        },
        "CommonRouter":    {
          "type":       "Router",
          "injections": [
            {
              "type":       "Validator",
              "injections": [
                {
                  "type": "AuthChecker"
                }
              ]
            }
          ]
        }
      },
      "types":        {
        "Server":          "./remote-access-layer/server.js",
        "Router":          "./remote-access-layer/backend/router.js",
        "Validator":       "./validation-layer/validator.js",
        "AuthChecker":     "./protection-layer/auth-checker.js",
        "CallboardFacade": "./business-logic-layer/callboard-facade.js",
        "UserFacade":      "./business-logic-layer/user-facade.js"
      }
    }
 */

class LayerCollector {
  static assemble(buildScheme) {
    let resultType = require(path.resolve(buildScheme.types[buildScheme.type]));

    let resultArgs = buildScheme.injections.map((arg) => {
      let argType = require(path.resolve(buildScheme.types[type]));
      return new argType();
    });

    return new resultType(...resultArgs);
  }

  static getInstance(buildScheme, type, injections) {
    if (buildScheme.types[type]) {
      let resultType = require(path.resolve(buildScheme.types[type]));
      let resultArgs = LayerCollector.getArguments(buildScheme, injections);

      return new resultType(...resultArgs);
    } else if (buildScheme.compositions[type]) {
      return LayerCollector.getInstance(buildScheme,
        buildScheme.compositions[type].type, buildScheme.compositions[type].injections);
    }

    throw new Error('Unknown type:', type);
  }

  static getArguments(buildScheme, args) {
    return args.injections.map((arg) => {
      if (typeof arg === 'string') {
        let argType = require(path.resolve(buildScheme.types[type]));
        return new argType();
      }
    });
  }
}

module.exports = LayerCollector;