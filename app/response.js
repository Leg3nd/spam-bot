'use strict';

class Response {
  call(method) {
    return async (req, res, next) => {
      try {
        let result = await this[method](req, res);
        if (result) {
          res.json(result)
        }
      } catch(error) {
        next(error);
      }
    }
  }
}

module.exports = Response;