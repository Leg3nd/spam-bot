'use strict';

const Response = require('../response');
let models = require('../sequelize');

class FormatController extends Response {
  async getSchedule(req, res) {
    try {
      if (!await models.schedule.count()) {
        await models.schedule.create({});
      }

      return {data: await models.schedule.findOne()};
    } catch (error) {
      res.status(400).json({errors: error.errors.map((item) => {
        return {field: item.path, message: item.message};
      })});
    }
  }

  async saveSchedule(req, res) {
    try {
      if (!await models.schedule.count()) {
        await models.schedule.create(req.body.schedule);
      } else {
        await models.schedule.update(req.body.schedule, {where: {}});
      }

      return {data: await models.schedule.findOne()};
    } catch(error) {
      res.status(400).json({
        errors: error.errors.map((item) => {
          return {field: item.path, message: item.message};
        })
      });
    }
  }
}

module.exports = new FormatController();
