'use strict';

const {Router} = require('express');
const controller = require('./schedule.controller');

const router = Router();

router.get('/', controller.call('getSchedule'));
router.put('/', controller.call('saveSchedule'));

module.exports = router;
