'use strict';

module.exports = (sequelize, DataTypes) => {
  let model = sequelize.define('schedule', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    frequency: {
      type:         DataTypes.ENUM('DAY', 'THREE_DAYS', 'WEEK', 'MONTH'),
      defaultValue: 'DAY',
      allowNull: false
    },
    hours: {
      type:      DataTypes.INTEGER,
      defaultValue: 12,
      validate: {
        isNumeric: true
      }
    },
    minutes: {
      type:      DataTypes.INTEGER,
      defaultValue: 0,
      validate: {
        isNumeric: true
      }
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    underscored: false
  });

  return model;
};
